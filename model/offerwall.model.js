module.exports = (sequelize, Sequelize) => {
	const Offerwall = sequelize.define('offerwall', {
	  user_id: {
		  type: Sequelize.STRING
	  },
	  tx_id: {
		  type: Sequelize.STRING
	  },
	  point_value: {
		  type: Sequelize.STRING
	  },
	  usd_value: {
		  type: Sequelize.STRING
	  },
	  offer_title: {
		  type: Sequelize.STRING
	  },
	  is_completed: {
		  type: Sequelize.STRING
	  },
	  complete_date: {
		  type: Sequelize.STRING
	  }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return Offerwall;
}