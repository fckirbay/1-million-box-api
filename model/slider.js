module.exports = (sequelize, Sequelize) => {
	const Slider = sequelize.define('slider', {
	  photo: {
		  type: Sequelize.STRING
	  },
	  title_1: {
		  type: Sequelize.STRING
	  },
	  title_2: {
		  type: Sequelize.STRING
	  },
	  title_3: {
		  type: Sequelize.STRING
	  },
	  button_title: {
		  type: Sequelize.STRING
	  },
	  button_link: {
		  type: Sequelize.STRING
	  },
	  order: {
		  type: Sequelize.STRING
	  },
	  lang: {
		  type: Sequelize.STRING
	  }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return Slider;
}