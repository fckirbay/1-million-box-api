const db = require('../config/db.config.js');
const config = require('../config/config.js');
const ROLEs = config.ROLEs; 
const User = db.user;
const Role = db.role;

checkDuplicateUserNameOrEmail = (req, res, next) => {
	// -> Check Username is already in use
	if(!(/^[a-z0-9]+$/i.test(req.body.username))) {
		res.status(200).send({ errorCode: 106 });
		//alert("Your password needs a minimum of four characters")
	} else if (req.body.username.length < 5 || req.body.username.length > 20) {
		res.status(200).send({ errorCode: 105 });
		//alert("Your password needs a minimum of four characters")
	} else if (req.body.password.length < 6) {
		res.status(200).send({ errorCode: 100 });
		//alert("Your password needs a minimum of four characters")
	} else if (req.body.password.search(/[a-z]/) < 0) {
		res.status(200).send({ errorCode: 101 });
		//alert("Your password needs a lower case letter")
	} else if(req.body.password.search(/[A-Z]/) < 0) {
		res.status(200).send({ errorCode: 102 });
		//alert("Your password needs an uppser case letter")
	} else  if (req.body.password.search(/[0-9]/) < 0) {
		res.status(200).send({ errorCode: 103 });
	    //alert("Your password needs a number")
	} else {
	    User.findOne({
			where: {
				username: req.body.username
			} 
		}).then(user => {
			if(user){
				res.status(200).send({ errorCode: 104 });
				return;
			}
			next();
		});
	}

	
}

checkRolesExisted = (req, res, next) => {	
	for(let i=0; i<req.body.roles.length; i++){
		if(!ROLEs.includes(req.body.roles[i].toUpperCase())){
			res.status(400).send("Fail -> Does NOT exist Role = " + req.body.roles[i]);
			return;
		}
	}
	next();
}

const signUpVerify = {};
signUpVerify.checkDuplicateUserNameOrEmail = checkDuplicateUserNameOrEmail;
signUpVerify.checkRolesExisted = checkRolesExisted;

module.exports = signUpVerify;