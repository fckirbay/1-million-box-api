const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Offerwall = db.offerwall;
const User = db.user;
const Notifications = db.notifications;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment');

exports.get = (req, res) => {

	/*Users.findOne({
		where: req.filters,
		attributes: ['id', 'name', 'username', 'email', 'first_name', 'last_name', 'createdAt', 'updatedAt', 'phone', 'photo', 'reference', 'ticket', 'clicks', 'won', 'lost', 'earnings', 'balance', 'reference_id', 'reference_count', 'blocked', 'blocked_reason', 'lang', 'currency', 'country', 'os', 'firebase', 'walkthrough', 'verification', 'verification_tries', 'purchasing', 'premium_membership', 'premium_expiration', 'is_notify']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})*/

	// NOTIFICATION GÖNDERİMİ EKLENECEK!!!

	var user_id = req.query.user_id;
	var tx_id = req.query.tx_id;
	var point_value = req.query.point_value;
	var usd_value = req.query.usd_value;
	var offer_title = req.query.offer_title;

	Offerwall.findOne({
		where: { user_id: user_id, tx_id: tx_id },
		attributes: ['user_id', 'tx_id', 'point_value', 'usd_value', 'offer_title', 'is_completed', 'complete_date']
	}).then(data => {
		if(data == null) {
			Offerwall.create({
				user_id: user_id,
				tx_id: tx_id,
				point_value: point_value,
				usd_value: usd_value,
				offer_title: offer_title,
				is_completed: 1,
				complete_date: moment().format("YYYY-MM-DD HH:mm:ss")
			}).then(saveResult => {
				User.increment(
					{ ticket: point_value }, 
					{ where: { id: user_id } 
				}).then(result =>
					res.status(200).json({
						"status": 200
					})
				).catch(err =>
					res.status(500).json({
						"error": err
					})
				)
			}).catch(err => {
				res.status(500).send("Fail! Error -> " + err);
			})
		} else {
			res.status(500).json({
				"error": "Duplicate!"
			});
		}
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}