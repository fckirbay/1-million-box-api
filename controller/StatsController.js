const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Stats = db.stats;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	
	Stats.findOne({
		where: req.filters,
		attributes: ['total_games', 'total_laps', 'total_coins']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}