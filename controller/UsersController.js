const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Users = db.users;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	req.filters['id'] = req.userId;
	
	if(!req.fields) {
		var attributes = ['id', 'name', 'username', 'email', 'first_name', 'last_name', 'createdAt', 'updatedAt', 'phone', 'photo', 'reference', 'ticket', 'clicks', 'won', 'lost', 'earnings', 'balance', 'reference_id', 'reference_count', 'blocked', 'blocked_reason', 'lang', 'currency', 'country', 'country_code', 'os', 'firebase', 'walkthrough', 'verification', 'verification_tries', 'purchasing', 'premium_membership', 'premium_expiration', 'is_notify']
	} else {
		var attributes = req.fields;
	}

	Users.findOne({
		where: req.filters,
		attributes: attributes
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}

exports.post = (req, res) => {

	Users.update(
	  { lang: req.body.lang },
	  { where: { id: req.userId } }
	)
	  .then(result =>
	    res.status(200).json({
			"status": 200,
			"token": jwt.sign({ id: req.userId, isVerify: 1, lang: req.body.lang }, config.secret, {
				expiresIn: 60 * 60 * 24 * 365 // expires in 24 hours
			})
		})
	  )
	  .catch(err =>
	    res.status(500).json({
	    	"status": 500,
			"error": err
		})
	  )
}

exports.updateos = (req, res) => {

	Users.update(
	  { os: req.body.os },
	  { where: { id: req.userId } }
	)
	  .then(result =>
	    res.status(200).json({
			"status": 200
		})
	  )
	  .catch(err =>
	    res.status(500).json({
	    	"status": 500,
			"error": err
		})
	  )
}

exports.updatedevice = (req, res) => {

	Users.update(
	  { firebase: req.body.firebase, os: req.body.os },
	  { where: { id: req.userId } }
	)
	  .then(result =>
	    res.status(200).json({
			"status": 200
		})
	  )
	  .catch(err =>
	    res.status(500).json({
	    	"status": 500,
			"error": err
		})
	  )
}