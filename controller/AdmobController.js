const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Clickrewarded = db.clickrewarded;
const User = db.user;
const Notifications = db.notifications;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment');

exports.get = (req, res) => {
	var user_id = req.userId;
	var token = req.query.token;

	Clickrewarded.findOne({
		where: { user_id: user_id, token: token, is_completed: 0, click_date: { [Op.between]: [moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss'), moment().format('YYYY-MM-DD HH:mm:ss')] } },
		attributes: ['id', 'user_id', 'token', 'type', 'is_completed', 'click_date', 'complete_date']
	}).then(data => {
		if(data == null) {
			res.status(200).json({
				"result": "No result!"
			})
		} else {
			Clickrewarded.update(
				{ is_completed: 1, complete_date: moment().format('YYYY-MM-DD HH:mm:ss') },
				{ where: { id: data.id } }
				).then(result =>
					User.increment(
							{ ticket: 1 }, 
							{ where: { id: user_id } 
						}).then(result =>
						    res.status(200).json({
								"status": 200
							})
						).catch(err =>
						    res.status(500).json({
								"error": err
							})
						)
				).catch(err =>
					res.status(200).send({ error: "Couldn't rewarded!" })
				)
		}
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}

exports.post = (req, res) => {

	//var user_id = req.body.user_id;
	var user_id = req.userId;
	var token = req.body.token;

	Clickrewarded.count({
	  where: { user_id: user_id, is_completed: 1, complete_date: { [Op.between]: [moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss'), moment().format('YYYY-MM-DD HH:mm:ss')] } }
	}).then(response => {
		if(response < 5) {
			Clickrewarded.create({
				user_id: user_id,
				token: token,
				type: 1,
				is_completed: 0,
				click_date: moment().format("YYYY-MM-DD HH:mm:ss")
			}).then(response => {
				res.status(200).json({
					"status": 200
				})
			}).catch(err => {
				res.status(500).send("Fail! Error -> " + err);
			})
		} else {
			res.status(200).json({
				"status": 201
			})
		}
	}).catch(err => {
		res.status(500).send("Fail! Error -> " + err);
	})

	
}