const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Games = db.games;
const Gamelaps = db.gamelaps;
const Users = db.users;
const Stats = db.stats;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	req.filters['user_id'] = req.userId;

	Games.findAll({
		where: req.filters,
		order: [['createdAt', 'DESC']],
		attributes: ['id', 'user_id', 'lap', 'coins', 'is_completed', 'createdAt'],
		limit: 100
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}

function createCard() {
    return Math.floor(Math.random() * 13) + 2 + ""+ Math.ceil(Math.random() * 4) +"";
}

exports.post = (req, res) => {


    Users.decrement(
		{ticket: 1}, 
		{ where: { id: req.userId } 
	}).then(result =>
		Games.create({
			user_id: req.userId,
			lap: 1,
			coins: 0,
			is_completed: 0
		}).then(game => {
			Gamelaps.create({
				game_id: game.id,
				card_1: createCard(),
				result: 0
			}).then(lap => {
				Stats.increment(
					{ total_games: 1, total_laps: 1 }, 
					{ where: { id: 1 } 
				}).then(result =>
				    res.status(200).json({
						"game": game,
						"lap": lap
					})
				).catch(err =>
				    res.status(500).json({
						"error": err
					})
				)
			}).catch(err => {
				res.status(500).send("Fail! Error -> " + err);
			})
		}).catch(err => {
			console.log(err);
			res.status(500).send("Fail! Error -> " + err);
		})
	).catch(err =>
		res.status(500).json({
			"error": err
		})
	)
	
}