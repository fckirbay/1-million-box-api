const db = require('../config/db.config.js');
const config = require('../config/config.js');
const References = db.references;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	req.filters['reference_id'] = req.userId;

	References.findAll({
		where: req.filters,
		order: [['id', 'DESC']],
		attributes: ['username', 'first_name', 'last_name', 'country', 'createdAt']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}