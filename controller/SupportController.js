const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Support = db.support;

const Op = db.Sequelize.Op;

const currentDate = new Date();

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {
	Support.findAll({
		where:  {
			user_id: req.userId,
			[Op.or]: [
		        {
		          'validity_date': { [Op.lte]: currentDate },
		        },
		        {
		          'validity_date': null,
		        },
		    ]
		},
		order: [['id', 'DESC']],
		attributes: ['user_id', 'message', 'owner', 'date', 'validity_date', 'is_viewed'],
		limit: 30
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}