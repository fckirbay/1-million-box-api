const env = require('./env.js');
 
const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  dialect: env.dialect,
  operatorsAliases: false,
 
  pool: {
    max: env.max,
    min: env.pool.min,
    acquire: env.pool.acquire,
    idle: env.pool.idle
  }
});
 
const db = {};
 
db.Sequelize = Sequelize;
db.sequelize = sequelize;
 
db.user = require('../model/user.model.js')(sequelize, Sequelize);
db.role = require('../model/role.model.js')(sequelize, Sequelize);
db.userjokers = require('../model/userjokers.model.js')(sequelize, Sequelize);
db.references = require('../model/references.model.js')(sequelize, Sequelize);
db.offerwall = require('../model/offerwall.model.js')(sequelize, Sequelize);
db.games = require('../model/games.model.js')(sequelize, Sequelize);
db.gamelaps = require('../model/gamelaps.model.js')(sequelize, Sequelize);
db.paymentmethods = require('../model/paymentmethods.model.js')(sequelize, Sequelize);
db.transactions = require('../model/transactions.model.js')(sequelize, Sequelize);
db.scratchwin = require('../model/scratchwin.model.js')(sequelize, Sequelize);
db.support = require('../model/support.model.js')(sequelize, Sequelize);
db.users = require('../model/users.model.js')(sequelize, Sequelize);
db.stats = require('../model/stats.model.js')(sequelize, Sequelize);
db.clickrewarded = require('../model/clickrewarded.model.js')(sequelize, Sequelize);
db.slider = require('../model/slider.model.js')(sequelize, Sequelize);

db.role.belongsToMany(db.user, { through: 'user_roles', foreignKey: 'roleId', otherKey: 'userId'});
db.user.belongsToMany(db.role, { through: 'user_roles', foreignKey: 'userId', otherKey: 'roleId'});

module.exports = db;